# はじめてのアプリケーションデプロイ

## アジェンダ
 1. Web UI ログイン
 1. Developer View の確認
 1. Project 作成
 1. アプリケーション作成
 1. アプリケーションの確認
 1. Liveness Probe 設定の追加
 1. オートヒールの動作の確認
 1. Podのスケール(並列実行数)調整

## 準備


<!-- Etherpad のURLを適宜編集  -->

<!-- 
```
講師によるガイド

環境上etherpad へ誘導し、アカウントにメールアドレスを記入してもらう

user1: toaraki@redhat.com
user2:
user3:
user4:
...

RHPDS workshop環境の共通パスワードは、openshift
ROSA の場合は、個別の人称方法に依存

```

* etherpad のデプロイ
* user1 - user'n' のリスとアップ


-->

### (ハンズオンの場合)ユーザアカウントの割り当て

**操作 1**
```
Etherpad にて、user1 ... の箇所へ、メールアドレスを入力してください。ユーザの重複はできないので、
空いている箇所へ記入をお願いします。
```

[Etherpad...メモツール](https://etherpad-gpte-etherpad.apps.cluster-mqls8.mqls8.sandbox1884.opentlc.com/p/20220228-starterkit-day1)

<!--
``` 画像で置き換え
...
user1: toaraki@redhat.com
user2:
user3:
user4:
...

```
-->

![Console Login](./images/WS042-2.png)
 
## 1. Web UI ログイン
WebブラウザよりOpenShift Web Conosole へアクセスします。

<!-- OpenShift コンソールのURLは環境に合わせて編集する  -->

[OpenShift Web console ](https://console-openshift-console.apps.cluster-mqls8.mqls8.sandbox1884.opentlc.com)

**操作 2**
```
アカウント：パスワードを入力し、ログインします。
```
![Console Login](./images/WS001.png)



## 2. Developer View の確認
ログインが完了すると、Developer View が表示されます。

**操作 3**
```
ツアーガイドが表示される場合、スキップしてください.
画面右上の'?' アイコンから"ガイド付きツアー"を呼び出すことでからいつでも表示できます.
```

![Console Login](./images/WS002.png)

* **参考**: ガイド付きツアー を任意に実行する
![Console Login](./images/WS003-3.png)

* **参考**: ２回目以降のログイン等の事情で、Administrator ビューが表示される場合は、左上部分のメニューより切り替えを行ってください。

![Console Login](./images/WS003-2.png)

## 3. Project 作成
まず、プロジェクトを作成します。任意の名称が指定できますが、今回は、アカウント-webapp (ex: user1 の場合、user1-webapp) としてください。

**操作 4**
```
画面中央部の"プロジェクト:すべてのプロジェクト" と表示されているドロップダウンリストを選択し、
リスト最下部の"プロジェクト作成"ボタンを押下します。
```

<!-- 画像 -->
![Console Login](./images/WS004-2.png)

**操作 5**
```
プロジェクトの作成ウィンドウにて、"名前" フィールドに、アカウント-webapp (ex: user1 の場合、user1-webapp) 
を入力し、作成ボタンを押下します。
```

<!-- 画像 -->
![Console Login](./images/WS006-2.png)

**確認**
```
画面が切り替わった際、"プロジェクト:アカウント-webapp" となっていることを確認します。
※操作の対象が、表示されているProject ( namespace ) であることを意味しています。
```

<!-- 画像 -->
![Console Login](./images/WS007-3.png)

## 4. アプリケーション作成
Python カタログを使用して、アプリケーションを作成します。

**操作 6**
```
左メニューから、"+追加" を選択します。(既に選択されている場合は、スキップして問題ありません。)
```
![Console Login](./images/WS007-5.png)

<!-- ### 開発カタログ　-->

**操作 7**
```
"すべてのサービス" を押下します。
```
![Console Login](./images/WS007-4.png)


<!-- ### 開発カタログの選択 -->

**操作 8**
```
"開発カタログ"画面の"キーワードでフィルター"に、`python` と入力し、カタログのリスとにフィルターを適用します。
```
![Console Login](./images/WS008.png)

**操作 9**
```
ビルダーイメージ：Pythonを押下します。
```
![Console Login](./images/WS008-2.png)

**操作 10**
```
カタログの説明が表示されるので、"アプリケーションの作成"ボタンを押下します。
```
![Console Login](./images/WS009.png)

**操作 11**
```
Source-to-Image(S2I) アプリケーションの作成　画面の GitリポジトリーURLに、
"https://github.com/openshift-katacoda/blog-django-py"
と入力し、画面下部の作成ボタンを押下します。
```
![Console Login](./images/WS010-2.png)

**確認**
```
画面がトポロジービューに変わります。デプロイ処理が完了するまで待ちとなります。
```
![Console Login](./images/WS011.png)

### デプロイ処理中の状態確認
Console の画面を探索し、どのような情報が参照できるか確認してください。
**当セクションの操作は、デプロイ処理の状態によって表示がことなります。資料中のスクリーンショットは
デプロイ処理中に順次処理されていく処理を取り上げていますが、状態によって表示内容が異なる(例えば、
ビルド処理が完了下後の、ビルドログの表示等 )場合があります。**


* Deploy ステータスの確認

```
表示されているアイコンを押下すると、右側に Deploymentの定義情報等を表示する画面が表示されます。
```
![Console Login](./images/WS012-2.png)

* リソースの状態の確認

```
Deployment情報表示部の内部タブ"リソース"を選択すると、リソースの状態を確認できます。
この資料の画像は、ビルド処理中となります。
```
![Console Login](./images/WS013-2.png)

* コンテナビルドログの確認

```
リソースタブ内の"ビルド"セクションにある"ログの表示"リンクを押下すると、ビルドログを確認することができます。
```
![Console Login](./images/WS014-3.png)
* コンテナビルドログ出力画面
![Console Login](./images/WS014.png)

* デプロイの完了

```
Pod のデプロイ処理が完了すると、トポロジービューのアイコンの表示が、濃い青枠で囲まれます。
```
![Console Login](./images/WS015.png)


## 5. アプリケーションの確認
外部からのアクセスが可能なアプリケーション ( route リソースを定義したアプリケーショイン）へは、ブラウザを介してアクセスすることが可能です。

**操作 12**
```
トポロジービューのアプリケーションアイコン右上の記号を押下します。
```
![Console Login](./images/WS016.png)

**確認**
```
サンプルアプリケーションの画面が表示されることを確認します。
 ※アプリケーションの作成操作から、10分以上経過してもアプリケーションが起動してこない場合は、講師へお声がけください。
```
![Console Login](./images/WS017.png)


## 6. Liveness Probe 設定の追加
コンテナアプリのヘルスチェック設定を行ないます。

**操作 13**
```
トポロジービューにて、Deployment 情報画面を開き（Pod のアイコンを押下 )、情報画面の上部に表示されている
"ヘルスチェック"セクションにある "ヘルスチェックの追加"リンクを押下します。
```
![Console Login](./images/WS015-2.png)

**操作 14**
```
"Liveness プローブの追加" ボタンを押下します。
```
![Console Login](./images/WS018-2.png)

**操作 15**
```
(今回はデフォルトの設定を使用します。)展開されたセクションの最下部にある"ㇾ"チェックを押下します。
```

![Console Login](./images/WS019-1.png)
(画面の続き)
![Console Login](./images/WS019-4.png)

**操作 16**
```
Liveness プローブの表示が、"追加済みのLivenessプローブ" (緑色の文字)となっていることを確認し、
画面下部の"追加" ボタンを押下します。
```
![Console Login](./images/WS019-5.png)

**確認**
```
Pod の再デプロイ処理が実行されるので、完了するのを待ちます。
```
![Console Login](./images/WS020.png)


## 7. オートヒールの動作の確認
擬似的に障害を発生させて、復旧されることを確認します。前節のLiveness プローブの設定が済んでない場合は、ここでの操作を行なったとしても回復は行なわれないので注意してください。

擬似的な障害として、コンテナへターミナルアクセスし、アプリケーションの構成ファイルを削除します。これによって、コンテナは、HTTPリターンコード 403 を返しますので、OpenShift のLiveness プローブが検出ししだい、自動的に復旧が行なわれます。

**確認**
```
トポロジービューのアプリケーションアイコン右上の記号から、アプリケーションへアクセスし、Blog アプリの画面が表示することを確認します。
```
![Console Login](./images/WS017.png)

**操作 17**
```
OpenShift のコンソールに戻り、トロポジービューからDeployment の情報を開きます。
続けて、Deployment 情報のPod セクションから、Pod 名（青文字表記のリンク)を押下します。
```
![Console Login](./images/WS015-3.png)

**操作 18**
```
Pod の詳細画面にて、"ターミナル"タブを選択します。
```
![Console Login](./images/WS021-2.png)

**操作 19**

```
擬似的に障害をおこします。ターミナルのシェル(コマンドライン)にて、下記のコマンドを入力します。

$ pwd 
/opt/app-root/src  
   ## カレントディレクトリが上記であることを確認.
$ ls
app.sh blog ...
   ## ファイルやディレクトリがあることを確認
$ rm -rf *
$ ls
  ## ファイルが削除されていることを確認
```

![Console Login](./images/WS022.png)
![Console Login](./images/WS023.png)

**確認**
```
Blog アプリケーションへアクセスし、"Forbidden (HTTP 403 )" が返されることを確認します。
```
![Console Login](./images/WS024.png)

**確認**
```
10秒程度の間隔で画面をリフレッシュして、アプリケーションの画面表示が正常になることを確認してください。
```
![Console Login](./images/WS025.png)


### 障害検出から回復までのイベントの確認

**操作 20**
```
OpenShift Console のトポロジービューより、Deployment の情報画面を展開し、PodセクションにあるPodのリンクをたどります。
```
![Console Login](./images/WS015-3.png)

**操作 21**
```
Pod の詳細画面にて"イベント"タブを押下します。
```
![Console Login](./images/WS021-3.png)


**確認**
```
Pod のイベント一覧の中に、Liveness Prove の失敗や、その後のリスタートのイベントが記録されていることを確認します。
表示の内容から、障害が検出され、回復が行なわれたことが分かります。
```
![Console Login](./images/WS028.png)


##  8. Podのスケール(並列実行数)調整
pod の並列実行数を調整し、スケールアウト/スケールインの調整が行なえることを確認します。OpenShift 内部の処理として、podのスケールに応じてserviceリソースのエントリーが自動的にアップデートされることを確認します。

**操作 22**
```
トポロジービューにてアプリケーションのアイコンを選択し、Deployment詳細の画面を開きます。
```
![Console Login](./images/WS029-2.png)

**操作 23**
```
Deployment詳細画面の "^"ボタンを1回押下し、Pod のスケールを+1 します。
```
![Console Login](./images/WS030.png)

**確認**
```
スケール処理が完了すると、濃い青円の中の数字が2となります。
```
* Pod のスケール調整中
![Console Login](./images/WS031-2.png)
* Pod のスケール調整完了
![Console Login](./images/WS032-2.png)

**<<注意>>**操作 23 〜 25 では動作の確認を行なうためにクライアントセッションのスティッキーを解除する操作を行ないます。通常のアプリケーションではクライアントセッションのスティッキー設定は要件を考慮し適切に設定されている必要があります。

**操作 24**
```
コマンドラインターミナル(OCP4.9ではTech pre-view)を起動するため、画面最上部、右部分の">_"アイコンを押下します。
```
![Console Login](./images/WS033-2.png)

**操作 25**
```
画面下部にコマンドラインターミナル起動のウィザードが表示される。プロジェクト名を"userxx-webapp"とし、開始ボタンを押下する。
```
![Console Login](./images/WS034-2.png)

**確認**
```
コマンドラインターミナルの起動が完了すると、Shellプロンプトが表示される。
```
![Console Login](./images/WS036-2.png)

**操作 25**

```
コマンドラインターミナルへ下記のコマンドを入力する

bash-4.4 $ oc annotate routes blog-django-py haproxy.router.openshift.io/balance='roundrobin'
bash-4.4 $ oc annotate routes blog-django-py haproxy.router.openshift.io/disable_cookies='true'

```
![Console Login](./images/WS037.png)

**確認**
```
サンプルアプリケーションの画面をリフレッシュし、接続先のPod がアクセス毎に交互になることを確認する。
```
![Console Login](./images/WS038-3.png)

### Service リソースの設定状態の確認

**操作 26**

```
トポロジービューにて、サンプルアプリケーション(①)のDeployment詳細画面内のリソースタブ(②)を
選択し、セクション内のservice のリンク(③)を押下します。
```

![Console Login](./images/WS039-2.png)

**操作 27**

```
service の詳細画面にて、"Pod"タブを押下します。
```

![Console Login](./images/WS040-2.png)


**確認**
```
Pod の一覧として、スケールと同数のPod がリストアップされていることを確認します。
```
![Console Login](./images/WS041-2.png)

**8. Podのスケール(並列実行数)調整**の操作によってPod数を増やす（減らす）することで、service のPod としてリストアップされるポッドのリストも自動的にメンテナンスされる様子を確認することができます。


<<以上>>
