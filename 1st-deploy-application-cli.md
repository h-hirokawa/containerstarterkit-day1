# はじめてのアプリケーションデプロイ

## アジェンダ
 1. CLI ログイン
 1. Project 作成
 1. アプリケーション作成
 1. アプリケーションの外部公開とアクセス確認
 1. Liveness Probe 設定の追加
 1. オートヒールの動作の確認
 1. Podのスケール(並列実行数)調整
 
## CLI ログイン
OpenShift ConsoleからWeb Terminalをご利用いただいている場合はこの手順は不要となります。

```sh
oc login -u <<Username>> -p openshift <<OpenShift API URL>>
```

OpenShift側がHTTPS通信に自己証明書を使っている場合、以下の警告が出ますので、接続先が正しいことを確認の上、`y` を押してください。
```
The server uses a certificate signed by an unknown authority.
You can bypass the certificate check, but any data you send to the server could be intercepted by others.
Use insecure connections? (y/n):
```

## Project 作成
まず、プロジェクトを作成します。任意の名称が指定できますが、今回は、アカウント-cli (ex: user1 の場合、user1-cli) とします。

```sh
oc new-project $(oc whoami)-cli
```

ここでは `oc whoami` コマンドを使って、使用ユーザー名を動的に指定しています。

また、プロジェクト作成済みの場合に、使用プロジェクトを指定するには以下のコマンドを実行してください。

```sh
oc project $(oc whoami)-cli
```

## アプリケーション作成
Python カタログを使用して、アプリケーションを作成します。
CLIからのアプリケーション作成には `oc new-app` マンドを利用することが可能です。

```
oc new-app python:3.9-ubi8~https://github.com/openshift-katacoda/blog-django-py.git
```

ここではPythonの *3.9-ubi8* image を利用して、 https://github.com/openshift-katacoda/blog-django-py.git をソースコードとして利用するアプリケーションの作成を実行しています。 

なお、使用できる登録済みアプリケーションテンプレートの一覧は `oc new-app --list` で取得することが可能です。

### Deploymentの存在確認

```sh
oc get deployment
```

Deployment `blog-django-py` が存在することを確認します。

### ビルドログの確認

```sh
oc get build
oc logs -f build/blog-django-py-1
```

上記でビルドの進捗状態をリアルタイムでフォローすることが可能です。 `Push successful` が表示されればビルド完了となります。

### アプリケーションPodの存在確認

```sh
oc get pod
```

上記の実行結果が以下のようになっていればOKです（アプリケーションPodのIDは環境ごとに異なります）

```
NAME                              READY   STATUS      RESTARTS   AGE
blog-django-py-1-build            0/1     Completed   0          4m8s
blog-django-py-xxxxxxx-xxxxxxxx   1/1     Running     0          41s
```

## アプリケーションの外部公開とアクセス確認
アプリケーションを公開するためには、CLIを使用してアプリをデプロイする場合は、以下のコマンドで明示的に外部からの通信を仲介するRouteを設定する必要があります。

```sh
oc expose service/blog-django-py
```

上記を実行後、以下のコマンドでURLを確認可能です。

```sh
oc get route
```

出力例(URLは環境ごとに異なります)
```
NAME             HOST/PORT                                                                    PATH   SERVICES         PORT       TERMINATION   WILDCARD
blog-django-py   blog-django-py-user*-cli.apps.cluster-*****.*****.sandbox****.opentlc.com           blog-django-py   8080-tcp                 None
```

上記のHOST情報を元にブラウザで `http://<<HOST>>` にアクセスしてください（HTTPSではアクセスできないため注意）。

![Console Login](./images/WS017.png)

## Liveness Probe 設定の追加
コンテナアプリの監視設定を行ないます。

まずEditorでYAML形式のLiveness Probeの設定定義を作成します
```sh
nano livenessProbe.yml
```

以下の内容でファイルを保存してください

```yaml
spec:
  template:
    spec:
      containers:
      - name: blog-django-py
        livenessProbe:
          failureThreshold: 3
          httpGet:
            path: /
            port: 8080
            scheme: HTTP
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 1
```

nanoを使用する場合、`Ctrl+X → Y → Enter` の順の入力でファイルを保存してEditorを終了できます。

上記の定義を以下の手順で現状のDeploymentに適用(Patch)します

```sh
# 適用前のDeployment設定をYAML形式で出力
oc get deployment/blog-django-py -o yaml

# 変更の適用
oc patch deployment/blog-django-py --patch-file ./livenessProbe.yml

# 変更後のデプロイメントの状態を確認
oc get deployment/blog-django-py -o yaml
```

適用前後のDeployment定義を見比べて、livenessProbeの設定が追加されていることを確認してください。

## オートヒールの動作の確認
擬似的に障害を発生させて、復旧されることを確認します。前節のLiveness プローブの設定が済んでない場合は、ここでの操作を行なったとしても回復は行なわれないので注意してください。

擬似的な障害として、コンテナへターミナルアクセスし、アプリケーションの構成ファイルを削除します。これによって、コンテナは、HTTPリターンコード 403 を返しますので、OpenShift のLiveness プローブが検出ししだい、自動的に復旧が行なわれます。

`oc rsh` を使用して、アプリPodのコンテナ環境内にShellでアクセスします。

```sh
oc rsh $(oc get pod -l deployment=blog-django-py -o name) bash
```

コンテナ内環境で以下を実行してください。
```sh
$ pwd 
/opt/app-root/src  
   ## カレントディレクトリが上記であることを確認.
$ ls
app.sh blog ...
   ## ファイルやディレクトリがあることを確認
$ rm -rf *
$ ls
  ## ファイルが削除されていることを確認
$ exit
```

確認: Blog アプリケーションへアクセスし、"Forbidden (HTTP 403 )" が返されることを確認します。

![Console Login](./images/WS024.png)


操作: 10秒程度の間隔で画面をリフレッシュして、アプリケーションの画面表示が正常になることを確認してください。

![Console Login](./images/WS025.png)

### Podの起動状態確認

```sh
oc describe $(oc get pod -l deployment=blog-django-py -o name)
```

上記を実行し、出力されたEvents欄を見て、Liveness probeの失敗とコンテナ再作成が起きていることを確認してください。

## Podのスケール(並列実行数)調整
pod の並列実行数を調整し、スケールアウト/スケールインの調整が行なえることを確認します。OpenShift 内部の処理として、レプリカ数に応じて自動的にPodが作成され、また外部からのアクセスも自動で適切に分散されることを確認します。

```sh
# レプリカ数を2に設定
oc scale deployment/blog-django-py --replicas=2
# 各PodへのアクセスをRound Robinで交互に行うように設定
oc annotate routes blog-django-py haproxy.router.openshift.io/balance='roundrobin'
# アクセス先をセッションごとに固定するためのCokkieを無効化
oc annotate routes blog-django-py haproxy.router.openshift.io/disable_cookies='true'
```

実際にPodの数がスケールされていることを確認します。

```sh
oc get pod
```

サンプルアプリケーションの画面をリフレッシュし、接続先のPod がアクセス毎に交互になることを確認しましょう。

![Console Login](./images/WS038-3.png)

<<以上>>
